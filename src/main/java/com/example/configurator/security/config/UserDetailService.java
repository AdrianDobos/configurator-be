package com.example.configurator.security.config;

import com.example.configurator.dto.UserDTO;
import com.example.configurator.entity.AppUser;
import com.example.configurator.entity.Role;
import com.example.configurator.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.BeanDefinitionDsl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service()
public class UserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;



    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AppUser> personCredentialModelOptional = userRepository.findByUserName(username);
        if (!personCredentialModelOptional.isPresent()) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        AppUser userModel = personCredentialModelOptional.get();
        String userName = userModel.getUserName();
        String password = userModel.getPassword();
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        //ROLE_ADMIN is important to be picked up by hasRole from @PreAuthorize in DummyConteoller
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_" +  userModel.getRole().name().toUpperCase());
        authorities.add(simpleGrantedAuthority);
        return new User(userName, password, authorities);
    }
    public AppUser register(UserDTO userDto) {
        List<AppUser> userModels = userRepository.findAll();
        if (userModels.size() > 0){
            userDto.setRole("USER");
        }else{
            userDto.setRole("ADMIN");
        }
        AppUser newUser = new AppUser();
        newUser.setUserName(userDto.getUserName());

        newUser.setRole(Role.valueOf(userDto.getRole()));
        newUser.setPassword(bcryptEncoder.encode(userDto.getPassword()));
        if (!ifUserIsRegistred(newUser.getUserName())){
            return userRepository.save(newUser);
        }
        return newUser;
    }

    private boolean ifUserIsRegistred(String email){
        List<AppUser> userModels = userRepository.findAll();
        for (AppUser userModel: userModels){
            if (userModel.getUserName().equals(email)){
                return true;
            }
        }
        return false;
    }

    public AppUser getLoggedUser(){
        Object principal = SecurityContextHolder. getContext(). getAuthentication(). getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            Optional<AppUser> personCredentialModelOptional = userRepository.findByUserName(username);
            if (!personCredentialModelOptional.isPresent()) {
                throw new UsernameNotFoundException("Invalid username or password.");
            }
            return personCredentialModelOptional.get();
        }
        throw new UsernameNotFoundException("Invalid username or password.");
    }

}

package com.example.configurator.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuotationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private double totalPrice;
    @OneToMany(cascade = CascadeType.REMOVE)
    private List<ProductEntity> productEntityList = new ArrayList<>();
    @ManyToOne
    private AppUser appUser;
}

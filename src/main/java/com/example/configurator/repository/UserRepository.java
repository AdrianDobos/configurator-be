package com.example.configurator.repository;

import com.example.configurator.entity.AppUser;
import com.example.configurator.entity.QuotationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByUserName(String username);


}

package com.example.configurator.repository;

import com.example.configurator.entity.ProductEntity;
import com.example.configurator.entity.QuotationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface QuotationRepository extends JpaRepository<QuotationEntity,Long> {

    Optional<QuotationEntity> findById(Long id);
    List<QuotationEntity> findQuotationEntityByAppUser_UserName(String username);


}

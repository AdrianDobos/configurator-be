package com.example.configurator.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Color {
    GOLDEN_OAK("GOLDEN_OAK"),
    WALNUT("WALNUT"),
    IVORY("IVORY"),
    MEDIUM_OAK("MEDIUM_OAK");

    private final String colorType;
}

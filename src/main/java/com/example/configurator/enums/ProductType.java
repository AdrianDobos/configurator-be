package com.example.configurator.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProductType {

    WINDOW("WINDOW"),
    DOOR("DOOR"),
    SLIDING_SYSTEM("SLIDING_SYSTEM");

    private final String productType;
}

package com.example.configurator.service;
import com.example.configurator.dto.UserCreateDTO;
import com.example.configurator.dto.UserDTO;

import java.util.List;

public interface UserServiceInterface {
    UserDTO create(UserCreateDTO createDTO);


    List<UserDTO>getAll();

    UserDTO getById(Long Id);
}

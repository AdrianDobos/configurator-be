//package com.example.configurator.service.mapper;
//
//import com.example.configurator.dto.ClientDTO;
//import com.example.configurator.entity.ClientEntity;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ClientMapper {
//    public ClientEntity toEntity(ClientDTO createDTO) {
//        return ClientEntity.builder()
//                .cnp(createDTO.getCnp())
//                .lastName(createDTO.getLastName())
//                .firstName(createDTO.getFirstName())
//                .address(createDTO.getAddress())
//                .build();
//    }
//    public ClientDTO toDTO(ClientEntity entity) {
//        return ClientDTO.builder()
//                .cnp(entity.getCnp())
//                .lastName(entity.getLastName())
//                .firstName(entity.getFirstName())
//                .build();
//    }
//}

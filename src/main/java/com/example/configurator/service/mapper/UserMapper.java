package com.example.configurator.service.mapper;

import com.example.configurator.dto.UserCreateDTO;
import com.example.configurator.dto.UserDTO;
import com.example.configurator.entity.AppUser;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public AppUser toEntity(UserDTO createDTO) {
        return AppUser.builder()
                .userName(createDTO.getUserName())
                .password(createDTO.getPassword())
                .build();
    }
    public UserDTO toDTO(AppUser entity) {
        return UserDTO.builder()
                .id(entity.getId())
                .userName(entity.getUserName())
                .build();
    }
}

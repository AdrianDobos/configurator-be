package com.example.configurator.service.mapper;

import com.example.configurator.dto.ProductDTO;
import com.example.configurator.entity.ProductEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {
    public ProductEntity toEntity(ProductDTO productDTO){
        return ProductEntity
                .builder()
                .productType(productDTO.getProductType())
                .price(productDTO.getPrice())
                .color(productDTO.getColor())
                .height(productDTO.getHeight())
                .width(productDTO.getWidth())
                .build();
    }
    public ProductDTO toDTO(ProductEntity productEntity){
        return ProductDTO
                .builder()
                .id(productEntity.getId())
                .productType(productEntity.getProductType())
                .price(productEntity.getPrice())
                .color(productEntity.getColor())
                .height(productEntity.getHeight())
                .width(productEntity.getWidth())
                .build();
    }
}

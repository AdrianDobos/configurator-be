package com.example.configurator.service.mapper;

import com.example.configurator.dto.ProductDTO;
import com.example.configurator.dto.QuotationDTO;
import com.example.configurator.entity.ProductEntity;
import com.example.configurator.entity.QuotationEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
@AllArgsConstructor
public class QuotationMapper {

    private final ProductMapper productMapper;

    public QuotationEntity toEntity(QuotationDTO quotationDTO) {
        List<ProductEntity> productEntityList = new ArrayList<>();
        quotationDTO.getProductDTOS().forEach(productDTO -> {
            productEntityList.add(productMapper.toEntity(productDTO));
        });
        return QuotationEntity.builder()
                .totalPrice(quotationDTO.getTotalPrice())
                .id(quotationDTO.getId())
                .productEntityList(productEntityList)
                .build();
    }

    public QuotationDTO toDTO(QuotationEntity quotationEntity) {
        List<ProductDTO> productDTOS = new ArrayList<>();
        quotationEntity.getProductEntityList().forEach(productEntity -> {
            productDTOS.add(productMapper.toDTO(productEntity));
        });
        return QuotationDTO.builder()
                .id(quotationEntity.getId())
                .totalPrice(quotationEntity.getTotalPrice())
                .userName(quotationEntity.getAppUser().getUserName())
                .productDTOS(productDTOS)
                .build();
    }

}

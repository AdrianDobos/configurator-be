package com.example.configurator.service;

import com.example.configurator.dto.DeleteProductDTO;
import com.example.configurator.dto.ProductDTO;

import java.util.List;

public interface ProductService {
    ProductDTO create(ProductDTO createDTO);
    List<ProductDTO> getAll();

    void delete(DeleteProductDTO deleteProductDTO);
}

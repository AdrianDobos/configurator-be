package com.example.configurator.service;


import com.example.configurator.dto.ProductDTO;
import com.example.configurator.dto.QuotationDTO;

import java.util.List;

public interface QuotationService {
   QuotationDTO create(QuotationDTO createDTO);

   QuotationDTO getQuotation(Long id);

   public List<ProductDTO> getProductListByQuotationId(Long quotationId);

   List<QuotationDTO> getQuotationsByLoggedUser();

}

package com.example.configurator.service.impl;


import com.example.configurator.dto.ProductDTO;
import com.example.configurator.dto.QuotationDTO;
import com.example.configurator.entity.QuotationEntity;
import com.example.configurator.repository.QuotationRepository;

import com.example.configurator.security.config.UserDetailService;
import com.example.configurator.service.QuotationService;
import com.example.configurator.service.mapper.QuotationMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class QuotationServiceImpl implements QuotationService {
    private final QuotationMapper quotationMapper;
    private final QuotationRepository quotationRepository;

    private final UserDetailService userDetailService;

    @Override
    public QuotationDTO create(QuotationDTO createDTO) {
        QuotationEntity toBeSaved = quotationMapper.toEntity(createDTO);
        toBeSaved.setAppUser(userDetailService.getLoggedUser());
        QuotationEntity created = quotationRepository.save(toBeSaved);
        return quotationMapper.toDTO(created);
    }

    @Override
    public QuotationDTO getQuotation(Long id) {
        Optional<QuotationEntity> quotationEntityOptional = quotationRepository.findById(id);
        if (quotationEntityOptional.isPresent()){
            return quotationMapper.toDTO(quotationEntityOptional.get());
        }else{
            throw new RuntimeException("The quotation don't exist");
        }
    }
    @Override
    public List<ProductDTO> getProductListByQuotationId(Long quotationId){
        return getQuotation(quotationId).getProductDTOS();
    }

    @Override
    public List<QuotationDTO> getQuotationsByLoggedUser() {
        List<QuotationEntity> quotationEntities =
                quotationRepository.findQuotationEntityByAppUser_UserName(
                        userDetailService.getLoggedUser().getUserName());

        List<QuotationDTO> quotationDTOS = new ArrayList<>();
        quotationEntities.forEach(quotationEntity -> {
           quotationDTOS.add(quotationMapper.toDTO(quotationEntity));
        });
        return quotationDTOS;
    }

}

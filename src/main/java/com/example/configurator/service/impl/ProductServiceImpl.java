package com.example.configurator.service.impl;

import com.example.configurator.dto.DeleteProductDTO;
import com.example.configurator.dto.ProductDTO;
import com.example.configurator.entity.ProductEntity;
import com.example.configurator.entity.QuotationEntity;
import com.example.configurator.repository.ProductRepository;
import com.example.configurator.repository.QuotationRepository;
import com.example.configurator.service.ProductService;
import com.example.configurator.service.mapper.ProductMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;


@Service
public class ProductServiceImpl implements ProductService {
    public static final double PRICE_ADJUSTMENT = 1000;
    private final ProductMapper mapper;
    private final ProductRepository repo;

    private final QuotationRepository quotationRepository;

    public ProductServiceImpl(ProductMapper mapper, ProductRepository repo, QuotationRepository quotationRepository) {
        this.mapper = mapper;
        this.repo = repo;
        this.quotationRepository = quotationRepository;
    }


    @Override
    public ProductDTO create(ProductDTO createDTO) {
        QuotationEntity quotationEntity = quotationRepository.getById(createDTO.getQuotationId());
        calculateProductPrice(createDTO);
        ProductEntity toBeSaved = mapper.toEntity(createDTO);
        ProductEntity created = repo.save(toBeSaved);
        quotationEntity.getProductEntityList().add(created);
        calculateQuotationPrice(quotationEntity);
        quotationRepository.save(quotationEntity);
        return mapper.toDTO(created);
    }

    private void calculateProductPrice(ProductDTO productDTO){
        Double totalPrice = productDTO.getHeight() * productDTO.getWidth() / PRICE_ADJUSTMENT;
        productDTO.setPrice(totalPrice);
    }

    @Override
    public List<ProductDTO> getAll(){
        List<ProductEntity> products =repo.findAll();
        List<ProductDTO>productDTOS=
                products
                        .stream()
                        .map(u-> mapper.toDTO(u))
                        .collect(Collectors.toList());
        return productDTOS;
    }

    @Override
    public void delete(DeleteProductDTO deleteProductDTO) {
        Optional<QuotationEntity>quotationEntityOptional=quotationRepository.findById(deleteProductDTO.getIdQuotation());
        if (quotationEntityOptional.isPresent()){
            AtomicReference<ProductEntity> productEntityToBeDeleted = new AtomicReference<>(new ProductEntity());
            quotationEntityOptional.get()
                    .getProductEntityList()
                    .forEach(productEntity -> {
                        if (productEntity.getId()== deleteProductDTO.getIdProduct()){
                            productEntityToBeDeleted.set(productEntity);
                        }
                    });
            quotationEntityOptional.get().getProductEntityList().remove(productEntityToBeDeleted.get());
            quotationRepository.save(quotationEntityOptional.get());
        }else{
            throw new RuntimeException("The product can't be deleted!");
        }
    }


    private void calculateQuotationPrice(QuotationEntity quotationEntity){
        AtomicReference<Double> totalPrice = new AtomicReference<>((double) 0);
        quotationEntity.getProductEntityList().forEach(productEntity -> {
            totalPrice.set(totalPrice.get() + productEntity.getPrice());
        });
        quotationEntity.setTotalPrice(totalPrice.get());
    }
}

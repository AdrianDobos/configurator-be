package com.example.configurator.service.impl;

import com.example.configurator.dto.UserCreateDTO;
import com.example.configurator.dto.UserDTO;
import com.example.configurator.entity.AppUser;
import com.example.configurator.repository.UserRepository;
import com.example.configurator.security.config.UserDetailService;
import com.example.configurator.service.mapper.UserMapper;
import org.apache.catalina.mapper.Mapper;
import org.springframework.stereotype.Service;



@Service
public class UserService extends UserDetailService{

    private final UserMapper mapper;
    private final UserRepository userRepository;

    public UserService(UserMapper mapper, UserRepository userRepository) {
        this.mapper = mapper;
        this.userRepository = userRepository;
    }

    public UserDTO create(UserDTO createDTO) {

        AppUser toBeSaved = register(createDTO);
        System.out.println(toBeSaved.getPassword());

        AppUser created = userRepository.save(toBeSaved);

        return mapper.toDTO(created);

    }

}

//package com.example.configurator.service.impl;
//
//import com.example.configurator.dto.ClientDTO;
//import com.example.configurator.entity.ClientEntity;
//import com.example.configurator.repository.ClientRepository;
//import com.example.configurator.service.ClientService;
//import com.example.configurator.service.mapper.ClientMapper;
//import lombok.AllArgsConstructor;
//import org.springframework.stereotype.Service;
//
//@AllArgsConstructor
//@Service
//public class ClientServiceImpl implements ClientService {
//    private final ClientMapper clientMapper;
//    private final ClientRepository clientRepository;
//
//    @Override
//    public ClientDTO create(ClientDTO createDTO){
//        ClientEntity clientEntity = clientRepository.save(
//                clientMapper.toEntity(createDTO));
//        return clientMapper.toDTO(clientEntity);
//
//    }
//
//}

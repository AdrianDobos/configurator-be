package com.example.configurator.controller;

import com.example.configurator.dto.DeleteProductDTO;
import com.example.configurator.dto.ProductDTO;
import com.example.configurator.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/product")
@AllArgsConstructor
public class ProductController {
    private final ProductService productService;
    @PostMapping
    public ResponseEntity<HttpStatus> create(@RequestBody ProductDTO productDTO) {
        ProductDTO created = productService.create(productDTO);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }
    @PatchMapping("/delete")
    public void delete(@RequestBody DeleteProductDTO deleteProductDTO) {
        productService.delete(deleteProductDTO);
    }
}

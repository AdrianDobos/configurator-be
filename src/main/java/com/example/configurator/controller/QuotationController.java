package com.example.configurator.controller;


import com.example.configurator.dto.ProductDTO;
import com.example.configurator.dto.QuotationDTO;
import com.example.configurator.service.ProductService;
import com.example.configurator.service.QuotationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/quotation")
@AllArgsConstructor
public class QuotationController {
    private final QuotationService quotationService;
    private final ProductService productService;
    @PostMapping
    public ResponseEntity<QuotationDTO> create(@RequestBody QuotationDTO quotationDTO) {
        quotationDTO.setProductDTOS(new ArrayList<>());
       QuotationDTO created = quotationService.create(quotationDTO);
        System.out.println(created.getId());
        return ResponseEntity.ok(created);
    }
    @GetMapping("/{id}")
    public ResponseEntity<QuotationDTO> getQuotation(@PathVariable(name = "id") long id) {
        QuotationDTO getQuotation = quotationService.getQuotation(id);
        return ResponseEntity.ok(getQuotation);
    }
    @GetMapping("")
    public ResponseEntity<List<QuotationDTO>> getAllQuotation() {
        List<QuotationDTO> quotationDTOS = quotationService.getQuotationsByLoggedUser();
        return ResponseEntity.ok(quotationDTOS);

    }

    @GetMapping("/products/{id}")
    public ResponseEntity<List<ProductDTO>> getProductListByQuotation(@PathVariable(name = "id") long id) {

            List<ProductDTO> productDTOList = quotationService.getProductListByQuotationId(id);
            return ResponseEntity.ok(productDTOList);

    }

}

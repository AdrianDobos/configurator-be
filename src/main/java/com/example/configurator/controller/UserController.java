package com.example.configurator.controller;

import com.example.configurator.dto.UserCreateDTO;
import com.example.configurator.dto.UserDTO;
import com.example.configurator.security.config.AuthTokenData;
import com.example.configurator.security.config.TokenProvider;
import com.example.configurator.service.impl.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {


    private final AuthenticationManager authenticationManager;

    private final TokenProvider jwtTokenUtil;

    private final UserService service;

    public UserController(AuthenticationManager authenticationManager, TokenProvider jwtTokenUtil, UserService service) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.service = service;
    }


    @PostMapping("/login")
    public ResponseEntity generateToken(@RequestBody UserDTO userDto) throws AuthenticationException {
        System.out.println("ceva");
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userDto.getUserName(),
                        userDto.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);
        System.out.println(token);
        return ResponseEntity.ok(new AuthTokenData(token));
    }
        @GetMapping("/user")
    public ResponseEntity<UserDTO> getUser()
            throws FileNotFoundException {
        return ResponseEntity.ok(UserDTO.builder().userName("test").password("test").build());
    }

    @PostMapping("/signup")
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO createDTO) {
        UserDTO created = service.create(createDTO);
        return ResponseEntity.ok(created);
    }

//    @GetMapping
//    public ResponseEntity<List<UserDTO>> getAll()
//            throws FileNotFoundException {
//        return ResponseEntity.ok(service.getAll());
//    }

//    @GetMapping("/{id}")
//    public ResponseEntity<UserDTO> getById(@PathVariable Long id, UsernamePasswordAuthenticationToken token) {
//        ConfiguratorUserDetails requestingUser = (ConfiguratorUserDetails) token.getPrincipal();
//        if (requestingUser.getId().equals(id)) {
//            return ResponseEntity.ok(service.getById(id));
//        } else {
//            throw new InvalidParameterException("Requested user is not your user.You d'ont have the permission");
//        }
//    }

}

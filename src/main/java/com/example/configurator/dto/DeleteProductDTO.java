package com.example.configurator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DeleteProductDTO {
    private Long idQuotation;
    private Long idProduct;
}

package com.example.configurator.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuotationDTO {
    private Long id;
    private double totalPrice;
    private List<ProductDTO> productDTOS = new ArrayList<>();
    private String userName;


}

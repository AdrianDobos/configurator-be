package com.example.configurator.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDTO {

    public Long id;

    public String userName;

    private String role;

    private String password;

    private List<QuotationDTO> quotations= new ArrayList<>();
}
